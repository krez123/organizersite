from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    #user Auth urls
    url(r'^login/$',  'createusers.views.login'),
    url(r'^auth/$',  'createusers.views.auth_view'),
    url(r'^logout/$', 'createusers.views.logout'),
    url(r'^loggedin/$', 'createusers.views.loggedin'),
    #url(r'^invalid/$', 'createusers.views.invalid_login'),
    url(r'^register/$', 'createusers.views.register_user'),
    url(r'^register_success/$', 'createusers.views.register_success'),
)
