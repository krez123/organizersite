from django.shortcuts import render
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from createusers.forms import MyRegistrationForm
# Create your views here.

def login(request):
    c = {}
    c.update(csrf(request))
    return render(request, 'createusers/login.html', c)

def auth_view(request):
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return HttpResponseRedirect('/accounts/loggedin')
        else:
            c = {}
            c.update(csrf(request))
            c.update({'errors': True})
            return render(request, 'createusers/login.html', c)
    else:
        c = {}
        c.update(csrf(request))
        return render(request, 'createusers/login.html', c)

def loggedin(request):
    return render(
        request,
        'mainpage/index.html',
        {'full_name': request.user.username}
    )
#def invalid_login(request):
#    return render_to_response('invalid_login.html')

def logout(request):
    auth.logout(request)
    return render(request, 'createusers/logout.html')

def register_user(request):
    args = {}
    args.update(csrf(request))
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/accounts/register_success')
        else:
            args['form']=form
    else:
        args['form'] = MyRegistrationForm()

    return render(request, 'createusers/register.html', args)

def register_success(request):
    return render(request, 'createusers/register_success.html')
