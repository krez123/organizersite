from django import forms
from models import UserProfile

class UserProfileForm(forms.ModelForm):
    name = forms.CharField(max_length=50,)
    surname = forms.CharField(max_length=50,)
    avatar = forms.FileField(required=False)
    birth_date = forms.DateField(required=False,)
    deskribe = forms.CharField(required=False, max_length=500,)
    class Meta:
        model = UserProfile
        fields = ('avatar', 'name', 'surname', 'birth_date', 'sex', 'deskribe')
