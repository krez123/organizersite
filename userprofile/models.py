from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from time import time
from django.core.urlresolvers import reverse

import logging
logr = logging.getLogger(__name__)

SEX = (
        (0, 'Male'),
        (1, 'Female'),
    )

def get_upload_file_name(instance, filename):
    return "uploaded_file/%s_%s" % (str(time()).replace('.','_'), filename )

class UserProfile(models.Model):
    friend = models.ForeignKey(User, related_name='friends', null=True, blank=True)
    user = models.OneToOneField(User)
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    deskribe = models.CharField(max_length=500, null=True)
    avatar = models.FileField(null=True, upload_to = get_upload_file_name)
    sex= models.IntegerField(choices=SEX, default=0)
    birth_date = models.DateField(null=True)

    def get_absolute_url(self):
        return reverse('profile-detail', args=[self.pk])

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
