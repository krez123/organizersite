from django.conf.urls import patterns, include, url
from userprofile.views import UserProfileDetailView

urlpatterns = patterns('',
        url(r'^profile/$', 'userprofile.views.user_profile'),
        url(r'^profile/(?P<pk>[0-9]+)/$', UserProfileDetailView.as_view(), name='profile-detail'),
        url(r'^profile/(?P<pk>[0-9]+)/add$', 'userprofile.views.user_profile_add'),
)
