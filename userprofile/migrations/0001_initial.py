# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import userprofile.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('surname', models.CharField(max_length=50)),
                ('deskribe', models.CharField(max_length=500, null=True)),
                ('avatar', models.FileField(default=b'uploads/python.jpg', null=True, upload_to=userprofile.models.get_upload_file_name)),
                ('sex', models.IntegerField(default=0, choices=[(0, b'Male'), (1, b'Female')])),
                ('birth_date', models.DateField(null=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
