# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('userprofile', '0002_auto_20170302_1841'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='friend',
            field=models.ForeignKey(related_name='friends', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
