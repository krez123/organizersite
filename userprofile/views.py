from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from forms import UserProfileForm
from django.contrib.auth.decorators import login_required
from django.views.generic.detail import DetailView
from models import UserProfile

@login_required
def user_profile(request):
    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/user/profile')
    else:
        user = request.user
        profile = user.profile
        form = UserProfileForm(instance=profile)

    args = {}
    args.update(csrf(request))

    args['form'] = form

    return render(request, 'userprofile/profile.html', args)

class UserProfileDetailView(DetailView):
    model=UserProfile

def user_profile_add(request, pk):
    userprofile = UserProfile.objects.get(pk=pk)
    userprofile.friend = request.user
    userprofile.save()
    return HttpResponseRedirect(userprofile.get_absolute_url())
