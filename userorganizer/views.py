from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from forms import UserOrganizerForm
from models import UserOrganizer
from django.contrib.auth.decorators import login_required
from userorganizer.models import UserOrganizer
from django.views.decorators.csrf import csrf_exempt
import datetime

@login_required
def user_organizer_event_add(request):
    # import pdb; pdb.set_trace()
    if request.method == 'POST':
        form = UserOrganizerForm(request.POST)
        if form.is_valid():
            form.instance.user = request.user
            obj = form.save()
            return HttpResponseRedirect('/user/event/{}'.format(obj.id))
    else:
        form = UserOrganizerForm()

    args = {}
    args.update(csrf(request))

    args['form'] = form

    return render(request, 'userorganizer/organizer.html', args)

# Create your views here.
@login_required
def user_organizer_event_edit(request,id):
    if request.method == 'POST':
        obj = UserOrganizer.objects.get(id=id)
        form = UserOrganizerForm(request.POST, instance=obj)
        if form.is_valid():
            form.instance.user = request.user
            obj = form.save()
            return HttpResponseRedirect('/user/event/{}'.format(obj.id))

    user = request.user
    profile = user.userorganizer_set.get(id=id)
    # profile = user.organizer
    form = UserOrganizerForm(instance=profile)
    args = {}
    args.update(csrf(request))
    args['form'] = form

    return render(request, 'userorganizer/organizer.html', args)

def user_organizer_event_list(request):
        return render(request, 'userorganizer/manage_event.html')

@login_required
@csrf_exempt
def search_event(request):

    corrent_user = request.user
    if request.method == "POST":
        search_date = request.POST['search_date']
    else:
        search_date = ''

    event_list = UserOrganizer.objects.filter(user_id = corrent_user, event_date__icontains = search_date).order_by('event_time')
#    event_list = UserOrganizer.objects.all()
    return render(request, 'userorganizer/ajax_organizer_search.html', {'event_list':event_list})
