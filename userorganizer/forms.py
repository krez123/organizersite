from django import forms
from models import UserOrganizer

class UserOrganizerForm(forms.ModelForm):
    eventabout = forms.CharField(required=False)
    event_time = forms.TimeField(widget=forms.TimeInput(format='%H:%M'))
    class Meta:
        model = UserOrganizer
        fields = ('id', 'eventname', 'event_date', 'event_time', 'eventabout')
