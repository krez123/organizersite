from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

import logging
logr = logging.getLogger(__name__)

class UserOrganizer(models.Model):
    user = models.ForeignKey(User)
    eventname = models.CharField(max_length=50)
    eventabout = models.CharField(max_length=200, null=True)
    event_date = models.DateField()
    event_time = models.TimeField()
