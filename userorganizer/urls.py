from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
        url(r'^event/list$', 'userorganizer.views.user_organizer_event_list'),
        url(r'^event/search/$', 'userorganizer.views.search_event'),
        url(r'^event/$', 'userorganizer.views.user_organizer_event_add'),
        url(r'^event/(?P<id>[0-9]+)/$', 'userorganizer.views.user_organizer_event_edit'),
)
