$(function(){

    $('#search').keyup(function() {

        $.ajax({
            type: "POST",
            url: "/friends/search/",
            data: {
                'search_text' : $('#search').val(),
                'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val()
            },
            success: searchSuccess,
            dataType: 'html'
        });

    });

        $('#search_date').keyup(function() {

            $.ajax({
                type: "POST",
                url: "/user/event/search/",
                data: {
                    'search_date' : $('#search_date').val(),
                    'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val()
                },
                success: searchSuccess,
                dataType: 'html'
            });

        });

});


function searchSuccess(data, textStatus, jqXHR)
{
    $('#search-results').html(data);
}
