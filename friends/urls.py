from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
        #url(r'^/$', 'friends.views.all_users'),
        url(r'^$', 'friends.views.friends_view'),
        url(r'^search/$', 'friends.views.search_friends'),
        #url(r'^event/(?P<id>[0-9]+)/$', 'userorganizer.views.user_organizer_event_list'),
)
