from django.db import models
from django.contrib.auth.models import User
# Create your models here.
import logging
logr = logging.getLogger(__name__)

class UserFriends(models.Model):
    user = models.ForeignKey(User)
