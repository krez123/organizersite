from django.shortcuts import render, render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from userprofile.models import UserProfile
from django.db.models import Q

@login_required
def friends_view(request):
    return render(request, 'friends/friends.html')

@login_required
@csrf_exempt
def search_friends(request):
    if request.method == "POST":
        search_text = request.POST['search_text']
    else:
        search_text = ''

    list_of_users = UserProfile.objects.filter(Q(name__icontains=search_text)|Q(name__icontains=search_text)|Q(user__username__icontains=search_text))

    return render(request, 'friends/ajax_friends_search.html', {'list_of_users':list_of_users})
